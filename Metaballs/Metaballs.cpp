#include "Metaballs.h"

#include <chrono>

namespace
{
	static int lastFrame = 0;
	static int frame = 0;
	static int minFrame = INT_MAX;
	static int maxFrame = INT_MIN;
	static int FPS = 0;
	static int renderTime = 0;
	static int calcTime = 0;
	static int qimageTime = 0;
	static float deltaTime = 1 / 60.0;

	std::chrono::steady_clock::time_point start, end;

	static float timeScale = 1;
}

Metaballs::Metaballs(QWidget *parent)
	: QMainWindow(parent)
{
	m_ui.setupUi(this);
	connectUI();
	getRenderers();
	twidth = m_ui.renderLabel->width();
	theight = m_ui.renderLabel->height();
	buf = new unsigned char[twidth * theight];

	for (int n = 0; n < 5; n++)
	{
		balls.push_back(100);
		balls.push_back(100);
		balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * (m_ui.maxSize->value() - m_ui.minSize->value()) + m_ui.minSize->value()));
		balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * 50));
		balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * 50));
	}
	clock.setAction([=]() {onThread(); });
	clock.setInterval(0);
	oneSecTimer.setInterval(100);
	oneSecTimer.setAction([=]() 
	{
		if (FPS < minFrame)
			minFrame = FPS;
		if (FPS > maxFrame)
			maxFrame = FPS;
		updateStats();
		lastFrame = frame;
	});
}

void Metaballs::start_stop()
{
	if (clock.running)
		clock.stop();
	else
		clock.start();

	if (oneSecTimer.running)
		oneSecTimer.stop();
	else
		oneSecTimer.start();
}

void Metaballs::rendererTextChanged(QString renderer)
{
	m_currentRenderer = renderer;
	resetStats();
}

void Metaballs::calculatorTextChanged(QString calculator)
{
	m_currentCalculator = calculator;
	resetStats();
}

void Metaballs::updateNumberOfBalls(int newNum)
{
	int dif = newNum - (balls.size() / 5);
	if (dif > 0)
		for (int n = 0; n < dif; n++)
		{
			balls.push_back(100);
			balls.push_back(100);
			balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * (m_ui.maxSize->value() - m_ui.minSize->value()) + m_ui.minSize->value()));
			balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * 50));
			balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * 50));
		}
	else
		for (int n = 0; n < abs(dif); n++)
			balls.erase(balls.begin(), balls.begin() + 5);
}

void Metaballs::updateTimeScale(int value)
{
	timeScale = /*1 + */(value / 100.0);
}

void Metaballs::resetStats()
{
	lastFrame = 0;
	frame = 0;
	minFrame = INT_MAX;
	maxFrame = INT_MIN;
	FPS = 0;
}

void Metaballs::resetAll()
{
	int size = balls.size() / 5;
	balls.clear();
	for (int n = 0; n < size; n++)
	{
		balls.push_back(100); 
		balls.push_back(100);
		balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * (m_ui.maxSize->value() - m_ui.minSize->value()) + m_ui.minSize->value()));
		balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * 50));
		balls.push_back(static_cast<int>(rand() / (float)RAND_MAX * 50));
	}
	twidth = m_ui.renderLabel->width();
	theight = m_ui.renderLabel->height();
	if (buf != nullptr)
		delete[] buf;
	buf = new unsigned char[twidth * theight];
	resetStats();
}

void Metaballs::connectUI()
{
	connect(m_ui.start_stop, &QPushButton::pressed, this, &Metaballs::start_stop);
	connect(m_ui.reset, &QPushButton::pressed, this, &Metaballs::resetAll);
	connect(m_ui.rendererSelection, &QComboBox::currentTextChanged, this, &Metaballs::rendererTextChanged);
	connect(m_ui.calculatorSelection, &QComboBox::currentTextChanged, this, &Metaballs::calculatorTextChanged);
	connect(m_ui.numberOfBalls, qOverload<int>(&QSpinBox::valueChanged), this, &Metaballs::updateNumberOfBalls);
	connect(m_ui.timeScale, &QSlider::valueChanged, this, &Metaballs::updateTimeScale);
}

void Metaballs::getRenderers()
{
	std::vector<Renderer**> renderers = PluginLoader<Renderer*>::getInstances("*Renderer.dll", "getRenderers");
	std::vector<int*> sizesrenderer = PluginLoader<int>::getInstances("*Renderer.dll", "getCount");
	for (int n = 0; n < renderers.size(); n++)
	{
		for (int i = 0; i < *sizesrenderer[n]; i++)
		{
			if (m_currentRenderer.isEmpty())
				m_currentRenderer = renderers[n][i]->getName();

			m_renderers.emplace(renderers[n][i]->getName(), renderers[n][i]);
			m_ui.rendererSelection->addItem(renderers[n][i]->getName());
		}
	}

	std::vector<Calculator**> calculators = PluginLoader<Calculator*>::getInstances("*Calculator.dll", "getCalculators");
	std::vector<int*> sizescalculator = PluginLoader<int>::getInstances("*Calculator.dll", "getCount");
	for (int n = 0; n < calculators.size(); n++)
	{
		for (int i = 0; i < *sizescalculator[n]; i++)
		{
			if (m_currentCalculator.isEmpty())
				m_currentCalculator = calculators[n][i]->getName();

			m_calculators.emplace(calculators[n][i]->getName(), calculators[n][i]);
			m_ui.calculatorSelection->addItem(calculators[n][i]->getName());
		}
	}

	m_currentRenderer = "CPU direct";
	m_currentCalculator = "CPU Multi Threading";
	m_ui.rendererSelection->setCurrentText(m_currentRenderer);
	m_ui.calculatorSelection->setCurrentText(m_currentCalculator);
}

void Metaballs::updateStats()
{
	m_ui.label_8->setText(QString("current frame #: %1").arg(frame));
	m_ui.label_5->setText(QString("current FPS: %1").arg(FPS));
	m_ui.label_7->setText(QString("max FPS: %1").arg(maxFrame));
	m_ui.label_6->setText(QString("min FPS: %1").arg(minFrame));

	m_ui.calcTime->setText(QString("calculation time: %1 ms").arg(calcTime));
	m_ui.renderTime->setText(QString("render time: %1 ms").arg(renderTime));
	m_ui.qimageTime->setText(QString("qimage time: %1 ms").arg(qimageTime));

	setWindowTitle(QString::number(balls.size() / 5));
}

void Metaballs::onThread()
{
	start = std::chrono::high_resolution_clock::now();
	auto renderer = m_renderers[m_currentRenderer];
	auto calculator = m_calculators[m_currentCalculator];

	auto s1 = std::chrono::high_resolution_clock::now();
	calculator->calculateImageBuffer(balls, buf, twidth, theight, timeScale * 10.0, deltaTime);
	auto e1 = std::chrono::high_resolution_clock::now();

	calcTime = ((e1 - s1) / std::chrono::milliseconds(1));


	auto s2 = std::chrono::high_resolution_clock::now();
	auto image = renderer->renderImage(twidth, theight, buf, twidth * theight);
	auto e2 = std::chrono::high_resolution_clock::now();

	renderTime = ((e2 - s2) / std::chrono::milliseconds(1));

	auto s3 = std::chrono::high_resolution_clock::now();
	m_ui.renderLabel->setPixmap(QPixmap::fromImage(*image).scaled(m_ui.renderLabel->size()));
	auto e3 = std::chrono::high_resolution_clock::now();

	qimageTime = ((e3 - s3) / std::chrono::milliseconds(1));

	frame++;
	delete image;

	end = std::chrono::high_resolution_clock::now();
	deltaTime = ((end - start) / std::chrono::milliseconds(1)) / 1000.0;
	FPS = 1.0 / deltaTime;
}
