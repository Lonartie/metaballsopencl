#pragma once

#include <QtWidgets/QMainWindow>
#include <QLabel>
#include <QPushButton>
#include "PluginLoader.h"
#include "../Core/Renderer.h"
#include "../Core/Calculator.h"
#include "Clock.h"
#include "ui_Metaballs.h"
#include <vector>
#include <map>

class Metaballs : public QMainWindow
{
	Q_OBJECT

public:

	Metaballs(QWidget *parent = Q_NULLPTR);

private slots:

	void start_stop();

	void rendererTextChanged(QString renderer);

	void calculatorTextChanged(QString calculator);

	void updateNumberOfBalls(int newNum);

	void updateTimeScale(int value);

	void resetAll();

private:

	void resetStats();

	void connectUI();

	void getRenderers();

	void updateStats();

	void onThread();

	int twidth;
	
	int theight;
	
	unsigned char* buf;

	Clock clock;

	Clock oneSecTimer;
	
	QImage* buffer;

	std::vector<float> balls;

	std::map<QString, Renderer*> m_renderers;

	std::map<QString, Calculator*> m_calculators;

	QString m_currentRenderer;

	QString m_currentCalculator;

	Ui::Metaballs m_ui;
};
