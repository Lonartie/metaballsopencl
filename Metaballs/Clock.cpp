#include "Clock.h"

Clock::Clock()
{
	connect(timer.get(), &QTimer::timeout, this, &Clock::call);
	timer->setInterval(0);
}

void Clock::setAction(std::function<void()> fnc)
{
	m_fnc = fnc;
}

void Clock::start()
{
	timer->start();
	running = true;
}

void Clock::setInterval(int ms)
{
	timer->setInterval(ms);
}

void Clock::stop()
{
	timer->stop();
	running = false;
}

void Clock::call()
{
	m_fnc();
}