#pragma once

#include <functional>
#include <memory>

#include <QTimer>
#include <QObject>
#include <memory>

class Clock : public QObject
{
	Q_OBJECT

public:

	Clock();

	bool running = false;

	void setAction (std::function<void()> fnc);

	void start();

	void setInterval(int ms);

	void stop();

private slots:

	void call();

private:

	std::function<void()> m_fnc;

	std::unique_ptr<QTimer> timer = std::make_unique<QTimer>();
};

