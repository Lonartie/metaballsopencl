#pragma once

#include <QString>
#include <QStringList>
#include <QLibrary>
#include <QDirIterator>
#include <QApplication>
#include <QDir>
#include <vector>

template<typename T>
class PluginLoader 
{
public:

	typedef T*(*creator)();
	
	static std::vector<T*> getInstances(QString filter, const char* method);
};

template<typename T>
std::vector<T*> PluginLoader<T>::getInstances(QString filter, const char * method)
{
	QDir path(QApplication::applicationDirPath());
	QStringList fileTypes;
	fileTypes << filter;
	QDirIterator it(path.absolutePath(), fileTypes, QDir::Files | QDir::NoSymLinks, QDirIterator::Subdirectories);
	std::vector<T*> instances;
	while (it.hasNext())
	{
		QString path = it.next();

		T* renderer = ((PluginLoader<T>::creator) QLibrary::resolve(path, method))();
		instances.push_back(renderer);
	}
	return instances;
}
