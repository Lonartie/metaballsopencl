#pragma once

#include "core_global.h"

#include <vector>

class CORE_EXPORT Calculator
{
public:

	virtual void calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime) = 0;

	virtual QString getName() = 0;
};


#define EXPORT_CALCULATORS_MANUAL(LIST, COUNT)						\
extern "C"															\
{																	\
	__declspec(dllexport) Calculator** getCalculators()				\
	{																\
		return LIST;												\
	}																\
																	\
	__declspec(dllexport) int* getCount()							\
	{																\
		return new int(##COUNT##);									\
	}																\
}													

#define EXPORT_CALCULATORS(LIST, COUNT)								\
EXPORT_CALCULATORS_MANUAL(new Calculator*[##COUNT##]##LIST, COUNT)


#define EXPORT_CALCULATOR(NAME)										\
EXPORT_CALCULATORS({new NAME##()},1)