#pragma once

#include "core_global.h"

#include <QtGui/QImage>

class CORE_EXPORT Renderer
{

public:

	virtual QImage* renderImage(int width, int height, unsigned char* buffer, long bufSize) = 0;

	virtual QString getName() = 0;

};


#define EXPORT_RENDERERS_MANUAL(LIST, COUNT)						\
extern "C"															\
{																	\
	__declspec(dllexport) Renderer** getRenderers()					\
	{																\
		return LIST;												\
	}																\
																	\
	__declspec(dllexport) int* getCount()							\
	{																\
		return new int(##COUNT##);									\
	}																\
}													

#define EXPORT_RENDERERS(LIST, COUNT)								\
EXPORT_RENDERERS_MANUAL(new Renderer*[##COUNT##]##LIST, COUNT)


#define EXPORT_RENDERER(NAME)										\
EXPORT_RENDERERS({new NAME##()},1)