#include "CpuMultiThreadingRenderer.h"

CpuMultiThreadingRenderer::CpuMultiThreadingRenderer()
{
}

QImage* CpuMultiThreadingRenderer::renderImage(int width, int height, unsigned char* buffer, long bufSize)
{
	QImage* buff2 = new QImage(width, height, QImage::Format_RGB666);
	int count = QThread::idealThreadCount();
	int dw = width / count;
	for (int n = 0; n < count; n++)
	{
		int start = n * dw;
		int end = (n + 1) * dw;
		threads.push_back(new std::thread([=]() 
		{
			renderPart (start, end, height, width, buffer, buff2);
		}));
	}

	waitForThreads();
	for (int n = 0; n < count; n++)
		delete threads[n];
	threads.clear();
	return buff2;
}

QString CpuMultiThreadingRenderer::getName()
{
	return "CPU Multi Threading";
}

void CpuMultiThreadingRenderer::renderPart(int start, int end, int height, int width, unsigned char * buffer, QImage * buf2)
{
	for (int y = 0; y < height; y++)
		for (int x = start; x < end; x++)
		{
			int value = static_cast<int>(buffer[y * width + x]);
			buf2->setPixelColor(x, y, QColor(value, value, value));
		}
}

void CpuMultiThreadingRenderer::waitForThreads()
{
	for (auto& thread : threads)
		thread->join();
}
