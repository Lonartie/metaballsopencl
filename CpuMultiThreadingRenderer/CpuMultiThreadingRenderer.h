#pragma once

#include "cpumultithreadingrenderer_global.h"

#include "../Core/Renderer.h"

#include <QImage>
#include <QString>
#include <QThread>

#include <vector>
#include <memory>
#include <thread>

class CPUMULTITHREADINGRENDERER_EXPORT CpuMultiThreadingRenderer : public Renderer
{
public:

	CpuMultiThreadingRenderer();

	virtual QImage* renderImage(int width, int height, unsigned char* buffer, long bufSize) override;


	virtual QString getName() override;

private:

	void renderPart(int start, int end, int height, int width, unsigned char* buffer, QImage* buf2);

	void waitForThreads();

	std::vector<std::thread*> threads;

};

EXPORT_RENDERER(CpuMultiThreadingRenderer)
