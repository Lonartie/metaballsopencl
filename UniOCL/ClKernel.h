#pragma once

#include "uniocl_global.h"

#include <string>
#include <typeinfo>
#include <vector>
#include <map>

class UNIOCL_EXPORT ClKernel
{

public /*static*/:

	static ClKernel getKernelWithName(std::string kernelName);

private /*static*/:

	static std::map<std::string, ClKernel> allKernels;

public:

	ClKernel() {};

	ClKernel(std::string name);

	void setKernelCode(std::string kernelCode);

	void loadKernelCodeFromFile(std::string filePath);
	
	std::string getKernelCode();

	std::string getKernelName();

private:

	std::string m_kernelCode;
	
	std::vector<const std::type_info*> m_argTypes;

	std::string m_kernelName;
};