#pragma once

#include "uniocl_global.h"

#include "ClResult.h"
#include "ClProgram.h"
#include "ClMemoryFlag.h"

#include <CL/cl.hpp>
#include <vector>
#include <map>
#include <iostream>

template<typename... arg>
class ClProgram
{
public:

	ClProgram() {}

	ClProgram(std::string kernelName, cl::Context* context, cl::Device device, cl::Program* program, std::vector<int> deviceFlags, std::vector<int> mainFlags)
		: m_program(program),
		m_context(context),
		m_device(device),
		m_deviceFlags(deviceFlags),
		m_mainFlags(mainFlags),
		m_kernelName(kernelName)
	{
		for (int n = 0; n < sizeof...(arg); n++)
			m_buffers.emplace(n, std::pair<cl::Buffer*, void*>(nullptr, nullptr));

		m_commandQueue = new cl::CommandQueue(*m_context, m_device, 0Ui64, &error);
		m_kernel = new cl::Kernel(*m_program, m_kernelName.c_str(), &error);
	};

	void writeBuffers(std::vector<unsigned long long> argSizes, arg...args)
	{
		for (auto& item : m_buffers)
			if (argSizes[item.first] > 0)
				delete item.second.first;

		m_bufferIndex = 0;
		createBuffers(m_deviceFlags, argSizes, args...);

		for (auto& buf : m_buffers)
			if (argSizes[buf.first] > 0)
				error = m_kernel->setArg(buf.first, *buf.second.first);
	};

	void exec(int iterations)
	{
		error = m_commandQueue->enqueueNDRangeKernel(*m_kernel, cl::NullRange, cl::NDRange(iterations), cl::NullRange);
		error = m_commandQueue->finish();
	}

	void readBuffers(std::vector<unsigned long long> argSizes, arg...args)
	{
		m_readBuffers.clear();
		m_bufferIndex = 0;
		
		readBuffers(m_mainFlags, argSizes, args...);

		//auto result = ClResult(m_readBuffers);
		m_readBuffers.clear();
	}

	ClResult operator() (int iterations, std::vector<unsigned long long> argSizes, arg...args)
	{
		if (m_commandQueue)
			delete m_commandQueue;
		if (m_kernel)
			delete m_kernel;

		m_bufferIndex = 0;
		m_commandQueue = new cl::CommandQueue(*m_context, m_device, 0Ui64, &error);
		createBuffers(m_deviceFlags, argSizes, args...);

		m_kernel = new cl::Kernel(*m_program, m_kernelName.c_str(), &error);

		for (auto& buf : m_buffers)
			error = m_kernel->setArg(buf.first, *buf.second.first);
		
		error = m_commandQueue->enqueueNDRangeKernel(*m_kernel, cl::NullRange, cl::NDRange(iterations), cl::NullRange);
		error = m_commandQueue->finish();

		m_bufferIndex = 0;
		readBuffers(m_mainFlags, argSizes, args...);
		
		auto result = ClResult(m_readBuffers);

		for (auto& item : m_buffers)
			delete item.second.first;
		//m_buffers.clear();
		m_readBuffers.clear();
		delete m_commandQueue;
		delete m_kernel;

		return result;
	};

private:

	template<typename First, typename...Rest> 
	void createBuffers(std::vector<int> deviceFlags, std::vector<unsigned long long> argSizes, First first, Rest...rest)
	{
		createBuffers<First>({ deviceFlags[0] }, { argSizes[0] }, first);
		argSizes.erase(argSizes.begin(), argSizes.begin() + 1);
		deviceFlags.erase(deviceFlags.begin(), deviceFlags.begin() + 1);
		createBuffers<Rest...>(deviceFlags, argSizes, rest...);
	}
	
	template<typename T>
	void createBuffers(std::vector<int> deviceFlags, std::vector<unsigned long long> argSizes, T value)
	{
		if (argSizes[0] != 0)
		{
			auto buffer = new cl::Buffer(
				(cl::Context) *m_context,
				(cl_mem_flags)(m_deviceFlags[m_bufferIndex] | m_mainFlags[m_bufferIndex]),
				(size_t)argSizes[0]);

			if (deviceFlags[0] == ClMemoryFlag::DeviceRead || deviceFlags[0] == ClMemoryFlag::DeviceReadWrite)
				error = m_commandQueue->enqueueWriteBuffer(*buffer, CL_TRUE, 0, argSizes[0], value);

			m_buffers[m_bufferIndex] = (std::pair<cl::Buffer*, void*>(buffer, value));
		}
		m_bufferIndex++;
	}

	template<typename First, typename...Rest>
	void readBuffers(std::vector<int> mainFlags, std::vector<unsigned long long> argSizes, First first, Rest...rest)
	{
		readBuffers<First>({ mainFlags[0] }, { argSizes[0] }, first);
		mainFlags.erase(mainFlags.begin(), mainFlags.begin() + 1);
		argSizes.erase(argSizes.begin(), argSizes.begin() + 1);
		readBuffers<Rest...>(mainFlags, argSizes, rest...);
	}

	template<typename T>
	void readBuffers(std::vector<int> mainFlags, std::vector<unsigned long long> argSizes, T value)
	{
		if (mainFlags[0] == ClMemoryFlag::HostRead || mainFlags[0] == ClMemoryFlag::HostReadWrite)
		{
			error = m_commandQueue->enqueueReadBuffer(*m_buffers[m_bufferIndex].first, CL_TRUE, 0, argSizes[0], value);
			m_readBuffers.emplace(m_bufferIndex, value);
		}
		m_bufferIndex++;
	}


	cl_int error = 0;

	int m_bufferIndex = 0;

	cl::CommandQueue* m_commandQueue = nullptr;

	std::map<int, void*> m_readBuffers;

	std::map<int, std::pair<cl::Buffer*, void*>> m_buffers;

	std::vector<int> m_deviceFlags, m_mainFlags;

	std::string m_kernelName;

	cl::Context* m_context = nullptr;

	cl::Device m_device;

	cl::Program* m_program = nullptr;

	cl::Kernel* m_kernel = nullptr;
};