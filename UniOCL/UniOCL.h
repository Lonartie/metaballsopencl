#pragma once

//#define __CL_ENABLE_EXCEPTIONS

#include "uniocl_global.h"

#include "ClDeviceType.h"
#include "ClKernel.h"
#include "ClOption.h"
#include "ClProgram.h"
#include "ClMemoryFlag.h"

#include <vector>
#include <QtCore/QString>
#include <iostream>
#include <string>

#include <CL/cl.hpp>

class UNIOCL_EXPORT UniOCL
{

public /*static*/:

	static QString getDefaultPlatformName();

	static std::vector<QString> getAllPlatformNames();

	static QString getDefaultDeviceName(ClDeviceType deviceType);

	static std::vector<QString> getAllDeviceNames(ClDeviceType deviceType);

	static cl::Platform getDefaultPlatform();

	static std::vector<cl::Platform> getAllPlatforms();

	static cl::Device getDefaultDevice(ClDeviceType deviceType);

	static std::vector<cl::Device> getAllDevices(ClDeviceType deviceType);

	template<typename...arg>
	static ClProgram<arg...> getProgram(std::string kernelName);

	template<typename...arg>
	static ClResult runProgram(std::string kernelName, int iterations, std::vector<int> argSizes, arg...args);

	template<typename... arg>
	static ClProgram<arg...> createProgram(ClOption programOptions,
		std::vector<int> deviceFlags,
		std::vector<int> mainFlags);

private /*static*/:

	static cl_device_type getType(ClDeviceType deviceType);

	static std::map<std::string, void*> m_allPrograms;

};

template<typename... arg>
ClProgram<arg...> UniOCL::createProgram(ClOption programOptions,
	std::vector<int> deviceFlags,
	std::vector<int> mainFlags)
{
	cl::Context* context = new cl::Context({ programOptions.getDevice() });
	cl::Program::Sources* sources = new cl::Program::Sources();
	auto kernelCode = programOptions.getKernelCode();
	sources->push_back({ kernelCode.c_str(), kernelCode.size() });

	cl::Program* program = new cl::Program(*context, *sources);
	if (program->build({ programOptions.getDevice() }) != CL_SUCCESS)
	{
		auto message = program->getBuildInfo<CL_PROGRAM_BUILD_LOG>(programOptions.getDevice());
		throw std::exception(message.c_str());
	}

	ClProgram<arg...>* clprogram = new ClProgram<arg...>(programOptions.getKernelName(), context, programOptions.getDevice(), program, deviceFlags, mainFlags);

	m_allPrograms.emplace(programOptions.getKernelName(), clprogram);

	return *clprogram;
}

template<typename...arg>
ClProgram<arg...> UniOCL::getProgram(std::string kernelName)
{
	return *static_cast<ClProgram<arg...>*>(m_allPrograms[kernelName]);
}

template<typename...arg>
ClResult UniOCL::runProgram(std::string kernelName, int iterations, std::vector<int> argSizes, arg...args)
{
	ClProgram<arg...> program = getProgram<arg...>(kernelName);
	return program(iterations, argSizes, args...);
}