#pragma once
#include "uniocl_global.h"

#include "ClDeviceType.h"
#include "ClKernel.h"

#include <vector>
#include <map>
#include <CL/cl.hpp>

class UNIOCL_EXPORT ClOption
{

public:

	ClOption(ClKernel& kernel);

	cl::Device getDevice();

	std::string getKernelCode();

	std::string getKernelName();

	void setDevice(ClDeviceType deviceType);

	void setDevice(cl::Device device);

private:

	cl::Device m_selectedDevice;

	ClKernel m_kernel;
};
