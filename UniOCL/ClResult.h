#pragma once

#include "uniocl_global.h"

#include <map>

class UNIOCL_EXPORT ClResult
{
public:

	ClResult(std::map<int, void*> data);

	template<typename T>
	T arg(int argnum);

private:

	std::map<int, void*> m_data;
};

template<typename T>
T ClResult::arg(int argnum)
{
	return static_cast<T>(m_data[argnum]);
}
