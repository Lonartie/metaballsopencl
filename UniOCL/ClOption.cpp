#include "ClOption.h"
#include "UniOCL.h"


ClOption::ClOption(ClKernel& kernel)
{
	m_kernel = kernel;
}

cl::Device ClOption::getDevice()
{
	return m_selectedDevice;
}

std::string ClOption::getKernelCode()
{
	return m_kernel.getKernelCode();
}

std::string ClOption::getKernelName()
{
	return m_kernel.getKernelName();
}

void ClOption::setDevice(ClDeviceType deviceType)
{
	m_selectedDevice = UniOCL::getDefaultDevice(deviceType);
}

void ClOption::setDevice(cl::Device device)
{
	m_selectedDevice = device;
}
