#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(UNIOCL_LIB)
#  define UNIOCL_EXPORT Q_DECL_EXPORT
# else
#  define UNIOCL_EXPORT Q_DECL_IMPORT
# endif
#else
# define UNIOCL_EXPORT
#endif
