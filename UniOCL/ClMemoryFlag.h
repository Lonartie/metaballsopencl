#pragma once
#include "uniocl_global.h"

enum UNIOCL_EXPORT ClMemoryFlag
{
	DeviceReadWrite = (1 << 0),
	DeviceRead = (1 << 2),
	DeviceWrite = (1 << 1),
	HostRead = (1 << 8),
	HostWrite = (1 << 7),
	HostNoAccess = (1 << 9),
	HostReadWrite = ((1 << 8) | (1 << 7))
};