#include "CpuMultiThreadingCalculator.h"

namespace
{
	float dist(int x1, int y1, int x2, int y2)
	{
		return static_cast<float>(sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)));
	}
}

CpuMultiThreadingCalculator::CpuMultiThreadingCalculator()
{
}

void CpuMultiThreadingCalculator::calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime)
{
	int threadCount = QThread::idealThreadCount();
	int dw = width / threadCount;

	for (int n = 0; n < threadCount; n++) 
	{
		threads.push_back(new std::thread([=]()
		{
			int start = n * dw;
			int end = (n + 1) * dw;
			calculateTile(start, end, width, height, buffer, balls);
		}));
	}

	for (int n = 0; n < balls.size() / 5; n++)
	{
		balls[5 * n + 0] += balls[5 * n + 3] * timeScale * deltaTime;
		balls[5 * n + 1] += balls[5 * n + 4] * timeScale * deltaTime;
		if (balls[5 * n + 0] >= width || balls[5 * n + 0] <= 0)
			balls[5 * n + 3] *= (-1);
		if (balls[5 * n + 1] >= width || balls[5 * n + 1] <= 0)
			balls[5 * n + 4] *= (-1);
	}

	waitForThreads();
	for (int n = 0; n < threadCount; n++)
		delete threads[n];
	threads.clear();
}

QString CpuMultiThreadingCalculator::getName()
{
	return "CPU Multi Threading";
}

void CpuMultiThreadingCalculator::calculateTile(int start, int end, int width, int height, unsigned char* buffer, std::vector<float> balls)
{
	for (int y = 0; y < height; y++)
	{
		for (int x = start; x < end; x++)
		{
			float sum = 0;
			for (int n = 0; n < balls.size() / 5; n++)
			{
				auto di = abs(dist(x, y, balls[5 * n + 0], balls[5 * n + 1]));
				auto size = balls[5 * n + 2];
				auto ps = size / di;
				sum += ps;
			}

			buffer[y * width + x] = (static_cast<int>(sin(sum / (2 * 3.1415)) * 255));
		}
	}
}

void CpuMultiThreadingCalculator::waitForThreads()
{
	for (auto& thread : threads)
		thread->join();
}
