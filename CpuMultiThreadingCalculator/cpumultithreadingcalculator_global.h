#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(CPUMULTITHREADINGCALCULATOR_LIB)
#  define CPUMULTITHREADINGCALCULATOR_EXPORT Q_DECL_EXPORT
# else
#  define CPUMULTITHREADINGCALCULATOR_EXPORT Q_DECL_IMPORT
# endif
#else
# define CPUMULTITHREADINGCALCULATOR_EXPORT
#endif
