#pragma once

#include "cpumultithreadingcalculator_global.h"

#include "../Core/Calculator.h"

#include <QString>
#include <QThread>

#include <vector>
#include <thread>

class CPUMULTITHREADINGCALCULATOR_EXPORT CpuMultiThreadingCalculator : public Calculator
{
public:
	CpuMultiThreadingCalculator();

	virtual void calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime) override;


	virtual QString getName() override;

private:

	void calculateTile(int start, int end, int width, int height, unsigned char* buffer, std::vector<float> balls);

	void waitForThreads();

	std::vector<std::thread*> threads;

};

EXPORT_CALCULATOR(CpuMultiThreadingCalculator)
