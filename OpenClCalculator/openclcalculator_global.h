#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(OPENCLCALCULATOR_LIB)
#  define OPENCLCALCULATOR_EXPORT Q_DECL_EXPORT
# else
#  define OPENCLCALCULATOR_EXPORT Q_DECL_IMPORT
# endif
#else
# define OPENCLCALCULATOR_EXPORT
#endif
