#pragma once

#include "openclcalculator_global.h"

#include "../Core/Calculator.h"
#include "../UniOCL/UniOCL.h"
#include "../UniOCL/ClDeviceType.h"
#include "../UniOCL/ClKernel.h"
#include "../UniOCL/ClOption.h"
#include "../UniOCL/ClProgram.h"
#include "../UniOCL/ClMemoryFlag.h"

#include <CL/cl.hpp>
#include <QString>
#include <QSize>
#include <string>
#include <map>

class OPENCLCALCULATOR_EXPORT OpenClCalculator : public Calculator
{

public /*static*/:

	static Calculator** getClCalculators();

	static int getClCount();

public:

	OpenClCalculator(QString name, cl::Device device);
	
	virtual void calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime) override;
	
	virtual QString getName() override;

private /*static*/:

	static int clDeviceCount;

	static std::map<QString, OpenClCalculator*> calcList;

	static Calculator** clCalculators;

	static cl::Device* clDevices;

	static cl::Device* initialize();

	static void setupDevice(cl::Device& device, int index);

private:

	void setupClProgram();

	bool m_bufferWritten = false;

	QSize m_lastSize;

	ClProgram<unsigned char* /*buffer*/,
		float* /*balls_data_in*/,
		float* /*balls_data_out*/,
		int* /*balls_size*/, 
		int* /*width*/, 
		int* /*height*/, 
		float* /*time scale*/,
		float* /*delta_time*/
	> program;

	QString m_name;

	cl::Device m_device;
};

EXPORT_CALCULATORS_MANUAL(OpenClCalculator::getClCalculators(), OpenClCalculator::getClCount())
