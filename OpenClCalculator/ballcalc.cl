float dist (int x1, int y1, float x2, float y2)
{
	return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

float myAbs(float value)
{
	if(value < 0)
		return value * (-1);
	return value;
}

kernel void BallCalculation(
	global uchar* buffer, 
	global float* balls, 
	global float* outBalls,
	global int* ballsSize,
	global int* width, 
	global int* height,
	global float* timeScale,
	global float* deltaTime)
{
	int w =  width[0];
	int h = height[0];
	int s = ballsSize[0];
	int id = get_global_id(0);
	int x = id;
	int y = 0;
	int totalCount = w * h;

	while (x > w)
	{
		x = x - w;
		y = y + 1;
	}

	float sum = 0;
	for (int n = 0; n < s; n++)
	{
		float di = myAbs(dist(x, y, balls[5 * n + 0], balls[5 * n + 1]));
		float size = balls[5 * n + 2];
		sum = sum + size / di;
	}

	buffer[id] = (convert_int(sin(sum / (2 * 3.1415)) * 255));


	barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

	if (id < s)
	{
		float dt = deltaTime[0];
		float posX = balls [5 * id + 0];
		float posY = balls [5 * id + 1];
		float velX = balls [5 * id + 3];
		float velY = balls [5 * id + 4];
		float tScale = timeScale[0];

		posX += velX * tScale * dt;
		posY += velY * tScale * dt;
		if (posX >= w || posX <= 0)
			velX *= (-1);
		if (posY >= h || posY <= 0)
			velY *= (-1);

		outBalls [5 * id + 0] = posX;
		outBalls [5 * id + 1] = posY;
		outBalls [5 * id + 2] = balls[5 * id + 2];
		outBalls [5 * id + 3] = velX;
		outBalls [5 * id + 4] = velY;
	}
}