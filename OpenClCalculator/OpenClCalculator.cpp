#include "OpenClCalculator.h"

#include <QCoreApplication>


int OpenClCalculator::clDeviceCount = 0;
Calculator** OpenClCalculator::clCalculators = nullptr;
cl::Device* OpenClCalculator::clDevices = nullptr;
std::map<QString, OpenClCalculator*> OpenClCalculator::calcList;

Calculator ** OpenClCalculator::getClCalculators()
{
	if (clDevices == nullptr)
		initialize();

	return clCalculators;
}

int OpenClCalculator::getClCount()
{
	if (clDevices == nullptr)
		initialize();

	return clDeviceCount;
}

OpenClCalculator::OpenClCalculator(QString name, cl::Device device)
	: m_name(name),
	m_device(device)
{
	setupClProgram();
}

void OpenClCalculator::calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime)
{
	auto wSize = new int(width);
	auto hSize = new int(height);
	auto bSize = new int(balls.size() / 5);
	auto tScale = new float(timeScale);
	auto dt = new float(deltaTime);

	if (m_lastSize.width() != width || m_lastSize.height() != height)
	{
		m_lastSize = QSize(width, height);
		program.writeBuffers({
			sizeof(unsigned char) * width * height,
			sizeof(float) * balls.size(),
			sizeof(float) * balls.size(),
			sizeof(int),
			sizeof(int),
			sizeof(int),
			sizeof(float),
			sizeof(float)
			}, buffer, balls.data(), balls.data(), bSize, wSize, hSize, tScale, dt);
		m_bufferWritten = true;
	}
	else
	{ 
		program.writeBuffers({ 
			0,
			sizeof(float) * balls.size(),
			sizeof(float) * balls.size(),
			sizeof(int),
			sizeof(int),
			sizeof(int),
			sizeof(float),
			sizeof(float)
			}, nullptr, balls.data(), balls.data(), bSize, wSize, hSize, tScale, dt);
	}

	program.exec(width * height);

	program.readBuffers({
			sizeof(unsigned char) * width * height, 0,
			sizeof(float) * balls.size(),0,0,0,0,0
		}, buffer, NULL, balls.data(), NULL, NULL, NULL, NULL, NULL);

	delete wSize; 
	delete hSize;
	delete bSize;
	delete tScale;
	delete dt;
}

QString OpenClCalculator::getName()
{
	return m_name;
}

cl::Device * OpenClCalculator::initialize()
{
	auto& devices = UniOCL::getAllDevices(ClDeviceType::ALL);

	for (int n = 0; n < devices.size(); n++)
		setupDevice(devices[n], n);

	clDeviceCount = calcList.size();
	clCalculators = new Calculator*[clDeviceCount];

	int index = 0;
	for (auto& item : calcList)
		clCalculators[index++] = item.second;

	return devices.data();
}

void OpenClCalculator::setupDevice(cl::Device& device, int index)
{
	auto platform = device.getInfo<CL_DEVICE_PLATFORM>();
	size_t nameSize = 0;
	clGetPlatformInfo(platform, CL_PLATFORM_NAME, 0, nullptr, &nameSize);
	char* platformName = new char[nameSize];
	clGetPlatformInfo(platform, CL_PLATFORM_NAME, nameSize, platformName, nullptr);
	std::string deviceName = device.getInfo<CL_DEVICE_NAME>();
	auto calcName = QString("%1::%2").arg(platformName).arg(deviceName.c_str());
	auto iter = calcList.find(calcName);
	delete[] platformName;
	if (iter != calcList.end() && calcList.size() > 0)
		return;

	OpenClCalculator* calculator = new OpenClCalculator(calcName, device);

	calcList.emplace(calcName, calculator);
}

void OpenClCalculator::setupClProgram()
{
	ClKernel kernel("BallCalculation");
	kernel.loadKernelCodeFromFile((QCoreApplication::applicationDirPath() + "/ballcalc.cl").toStdString());

	ClOption option(kernel);
	option.setDevice(m_device);

	program = UniOCL::createProgram<unsigned char*, float*, float*, int*, int*, int*, float*, float*>(option,
		{ ClMemoryFlag::DeviceWrite, ClMemoryFlag::DeviceRead, ClMemoryFlag::DeviceWrite, ClMemoryFlag::DeviceRead, ClMemoryFlag::DeviceRead, ClMemoryFlag::DeviceRead, ClMemoryFlag::DeviceRead, ClMemoryFlag::DeviceRead },
		{ ClMemoryFlag::HostRead   , ClMemoryFlag::HostWrite , ClMemoryFlag::HostRead   , ClMemoryFlag::HostWrite , ClMemoryFlag::HostWrite , ClMemoryFlag::HostWrite , ClMemoryFlag::HostWrite , ClMemoryFlag::HostWrite  });
}
