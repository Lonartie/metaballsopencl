#pragma once

#include "cpurenderer_global.h"

#include "../Core/Renderer.h"

#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QColor>
#include <QImage>

class CPURENDERER_EXPORT CpuRenderer : public Renderer
{
public:


	virtual QImage* renderImage(int width, int height, unsigned char* buffer, long bufSize) override;


	virtual QString getName() override;
};

EXPORT_RENDERER(CpuRenderer)
