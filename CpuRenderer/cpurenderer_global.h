#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(CPURENDERER_LIB)
#  define CPURENDERER_EXPORT Q_DECL_EXPORT
# else
#  define CPURENDERER_EXPORT Q_DECL_IMPORT
# endif
#else
# define CPURENDERER_EXPORT
#endif
