#include "CpuRenderer.h"


QImage* CpuRenderer::renderImage(int width, int height, unsigned char* buffer, long bufSize)
{
	QImage* buf = new QImage(buffer, width, height, width, QImage::Format_Grayscale8);
	return buf;
}

QString CpuRenderer::getName()
{
	return "CPU direct";
}
