#pragma once

#include "cpucalculator_global.h"

#include "../Core/Calculator.h"

#include <QString>
#include <vector>

class CPUCALCULATOR_EXPORT CpuCalculator : public Calculator
{
public:
	
	CpuCalculator();
	
	virtual void calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime) override;


	virtual QString getName() override;

};

EXPORT_CALCULATOR(CpuCalculator)
