#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(CPUCALCULATOR_LIB)
#  define CPUCALCULATOR_EXPORT Q_DECL_EXPORT
# else
#  define CPUCALCULATOR_EXPORT Q_DECL_IMPORT
# endif
#else
# define CPUCALCULATOR_EXPORT
#endif
