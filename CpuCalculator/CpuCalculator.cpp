#include "CpuCalculator.h"


namespace
{
	float dist(int x1, int y1, int x2, int y2)
	{
		return static_cast<float>(sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)));
	}
}


CpuCalculator::CpuCalculator(){}

void CpuCalculator::calculateImageBuffer(std::vector<float>& balls, unsigned char* buffer, int width, int height, float timeScale, float deltaTime)
{
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			float sum = 0;
			for (int n = 0; n < balls.size() / 5; n++)
			{
				auto di = abs(dist(x, y, balls[5 * n + 0], balls[5 * n + 1]));
				auto size = balls[5 * n + 2];
				auto ps = size / di;
				sum += ps;
			}

			buffer[y * width + x] = (static_cast<int>(sin(sum / (2 * 3.1415)) * 255));
		}
	}

	for (int n = 0; n < balls.size() / 5; n++)
	{
		balls[5 * n + 0] += balls[5 * n + 3] * timeScale * deltaTime;
		balls[5 * n + 1] += balls[5 * n + 4] * timeScale * deltaTime;
		if (balls[5 * n + 0] >= width || balls[5 * n + 0] <= 0)
			balls[5 * n + 3] *= (-1);
		if (balls[5 * n + 1] >= width || balls[5 * n + 1] <= 0)
			balls[5 * n + 4] *= (-1);
	}
}

QString CpuCalculator::getName()
{
	return "CPU";
}
